module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Heidi\'s Silly Sounds',
    meta: [
      { name: 'robots', content: 'noindex,nofollow' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://cdn.polyfill.io/v2/polyfill.js?features=default,fetch' }
    ],
    noscript: [
      { innerHTML: 'This website requires JavaScript.' }
    ]
  },
  css: [],
  /*
  ** Use more Vue plugins.
  */
  plugins: [],
  /*
  ** Add PWA capabilities
  */
  modules: [
    '@nuxtjs/pwa'
  ],
  /*
  ** Add Firebase Authorization to middleware.
  */
  router: {},
  /*
  ** Mode set to Single Page App when generating static pages.
  */
  mode: 'spa',
  /*
  ** Disable the progress bar
  */
  loading: false,
  /*
  ** Customize app manifest
  */
  manifest: {
    name: 'Heidi\'s Silly Sounds',
    theme_color: '#000000'
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: [],
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
